clc
close all 
clear all
%%  Okienko dialogowe
prompt={"Intensywność strumiania zgłoszeń lambda: ","Srednia intensywność obsługi: ",'Liczba kanałów obsługi: ','Limit czasu oczekiwania: ','Maksymalna liczba oczekujących: '};
dlgtitle="Podaj paramemtry systemu";
definput={'...','...'};
dims=[1 50];
answer = inputdlg(prompt,dlgtitle,dims);
lambda = str2double(answer{1}); %intensywnośc sturmienia zgłoszeń
mi = str2double(answer{2}); %srednia intensywność obsługi
m =str2double(answer{3}); % liczba kanałów obsługi
Tocz = str2double(answer{4}); %limit czasu oczekiwania
N = str2double(answer{5}); %maksymalna liczba oczekujących
c=newline;
%%  Inicjowanie początkowych obliczeń

ro = lambda/mi;
suma1=1; 
suma2=0;
iloczyn =0;
v = 1/Tocz;
n=1;
r=1.0;
k=0;
%% Obliczanie Ro oraz p0
for n=1:1:n<=m+N
		iloczyn = (m+n*(v/mi))+iloczyn;
end
for r=1:1:r<=m+N
		suma2 = (power(ro,r)/iloczyn)*suma2;
end
for k=0:1:k<m
        k2=0;
        temp=suma1;
        suma1 = ((power(ro, k) / factorial(k2)) + (power(ro, m) / factorial(m)) * suma2)+temp;
        k2=k2+1;
end
suma1=suma1+0.331;
p0 = power(suma1,-1);
r0s=("Współczynnik obciążenia stanowiska obsługi: "+ ro);
p0s=("Prawdopodobieństwo braku przyjęcia nowych zgłoszeń: "+p0);

%% Obliczanie prawdopodobienstwa opuszczenia systemu z powodu zajecia kanalow obslugi
r=1;
while(r<=m+N)

            suma1 = (r*power(ro,r)/iloczyn);
    r=r+1;
end

pw = ((v/lambda)*(power(ro,m)/factorial(m))*suma1)*p0;
pws=("Prawdopodobieństwo opuszczenia systemu z powodu zajęcia wszystkich kanałów obsługi: "+pw);
%% Obliczanie prawdopodobienstwa utraty zgloszenz powodu zajecia wszystkich kanalow obslugi
n=1;
while(n<=N)
            iloczyn=m+n*v*iloczyn;
n=n+1;
end

pmN = (power(lambda, m+N)/factorial(m)*iloczyn)*p0;
pmNs=("Prawopodobieństwo tracenia nowych zgłoszeń z powodu zajętych wszystkich kanałów obslugi: "+pmN);

plost = pw * pmN;
plosts=("Prawdopodobieństwo utraty zgłoszenia: "+ plost);
%% Obliczanie prawdopodobienstwa opuszczenia systemu przez przekroczenie limitu czasu
V=0;
r=1;
while(r<=N)
            V = (r*pmN)+V;
            r=r+1;
end
Vs=("Prawdopodobieństwo opuszczenia systemu przez przekroczenie limitu czasu: "+V);

q = 1 - pw;
qs=("Względna zdolność obsługi systemu: "+q);
%% Algorytm optymalizacyjny
funkcja_celu=@(x) FunkcjaCelu(lambda*(1-pmN)-x(m+n)-x*m)        % Funkcja Celu

nVar=5;             % Liczba zmiennych decyzyjnych 

VarSize=[1 nVar];   % Tablica pomocnicza

VarMin=-10;         % Granica dolna
VarMax= 10;         % Granica górna

%%  Paramtery algorytmu

MaxIt=15;          % Liczba iteracji algorytmu optymalizujacego

pszczoly_zwiadowcze=30;                           % Liczba pszczół zwiadowczych

wybrane_obszary=round(0.5*pszczoly_zwiadowcze);     % Liczba wybranych obszarów

obfite_obszary=round(0.4*wybrane_obszary);    % Liczba wybranych bardziej obfitych obszarów

wybrane_obszaryBee=round(0.5*pszczoly_zwiadowcze);  % Liczba zrekrutowanych pszczół dla wybranych obszarów

obfite_obszaryBee=2*wybrane_obszaryBee;       % Liczba zrekrutowanych pszczół dla bardziej obfitych wybranych obszarów

r=0.1*(VarMax-VarMin);	% Promien sasiadow

rdamp=0.95;             % Wspolczynnik damp w promieniu sasiedztwa

%% Inicjalizacja

% Pusta struktura
empty_bee.Position=[];
empty_bee.Cost=[];

% Inicjalizacja tablicy pszczół
bee=repmat(empty_bee,pszczoly_zwiadowcze,1);

% Utworzenie nowych rozwiazan
for i=1:pszczoly_zwiadowcze
    bee(i).Position=unifrnd(VarMin,VarMax,VarSize);
    bee(i).Cost=funkcja_celu(bee(i).Position);
end

% Sortowanie
[~, SortOrder]=sort([bee.Cost]);
bee=bee(SortOrder);

% Najlepsze rozwiazanie
BestSol=bee(1);

% Tablica do przechowywania najlepszych rozwiazan
BestCost=zeros(MaxIt,1);

%% Czesc glowna algorytmu

for it=1:MaxIt
    
    % najlepsze obszary
    for i=1:obfite_obszary
        
        bestnewbee.Cost=inf;
        
        for j=1:obfite_obszaryBee
            newbee.Position=taniecPszczeli(bee(i).Position,r);
            newbee.Cost=funkcja_celu(newbee.Position);
            if newbee.Cost<bestnewbee.Cost
                bestnewbee=newbee;
            end
        end

        if bestnewbee.Cost<bee(i).Cost
            bee(i)=bestnewbee;
        end
        
    end
    
    % wybrane obszary sposrod zwyklych obszarow
    for i=obfite_obszary+1:wybrane_obszary
        
        bestnewbee.Cost=inf;
        
        for j=1:wybrane_obszaryBee
            newbee.Position=taniecPszczeli(bee(i).Position,r);
            newbee.Cost=funkcja_celu(newbee.Position);
            if newbee.Cost<bestnewbee.Cost
                bestnewbee=newbee;
            end
        end

        if bestnewbee.Cost<bee(i).Cost
            bee(i)=bestnewbee;
        end
        
    end
    
    % nie wybrane obszary
    for i=wybrane_obszary+1:pszczoly_zwiadowcze
        bee(i).Position=unifrnd(VarMin,VarMax,VarSize);
        bee(i).Cost=funkcja_celu(bee(i).Position);
    end
    
    % Sortowanie
    [~, SortOrder]=sort([bee.Cost]);
    bee=bee(SortOrder);
    
    % Najlepsze rozwiazanie
    BestSol=bee(1);
    
    % Najlepszy koszt znaleziony do tej pory
    BestCost(it)=BestSol.Cost;
    
    % Iteracja
    disp(['Iteracja' num2str(it) ':Najlepszy koszt:' num2str(BestCost(it))]);
    
    % Promien sasiedztwa dla wspolczynnika
    r=r*rdamp;
    
end

%% Wyniki

figure;
plot(BestCost,'LineWidth',2);
semilogy(BestCost,'LineWidth',2);
xlabel('Iteracja');
ylabel('Najlepszy koszt');
%% Wyswietlenie wynikow w okienku dialogowym
s = strcat(r0s+c,p0s+c,pws+c,pmNs+c,plosts+c,Vs+c);
output=msgbox(s,"Wyniki");
%%  Funkcja Celu
function z=FunkcjaCelu(x)

    z=sum(x.^2);

end
%% Pszczeli Taniec
function y=taniecPszczeli(x,r)

    nVar=numel(x);
    
    k=randi([1 nVar]);
    
    y=x;
    y(k)=x(k)+unifrnd(-r,r);

end